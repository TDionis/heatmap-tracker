'use strict';

angular
  .module('heatmap-tracker')
  .config(function ($routeProvider, $httpProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'tracker.html',
        controller: 'TrackerCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  })
  .run(function ($rootScope) { //, dataService
  });

angular
  .module('heatmap-tracker')
  .filter('firstElement', function() {
    return function(obj) {
      return obj[Object.keys(obj)[0]];
    };
});

angular
  .module('heatmap-tracker')
  .controller('TrackerCtrl', function ($scope) { //, dataService
    
    var gd = function(date) {
      return (new Date(date)).toISOString().slice(0, 10)
    }

    $scope.data = JSON.parse(localStorage.getItem('data')) || []; /*[{
      project: 'Project 1',
      date: '2015-09-07',
      value: 2
    },{
      project: 'Project 1',
      date: '2015-09-09',
      value: 1
    },{
      project: 'Project 2',
      date: '2015-09-11',
      value: 1
    }];*/

    $scope.clickSquare = function(proj, day) {
      // $scope.heatmap[proj][day] = ($scope.heatmap[proj][day] + 1) % 3;

      var squareIdx = null;
      angular.forEach($scope.data, function(row, idx) {
        if (row.project == proj && gd(row.date) == gd(day))
          squareIdx = idx;
      });

      if (squareIdx !== null) {
        $scope.data[squareIdx].value = ($scope.data[squareIdx].value + 1) % 4;
        if ($scope.data[squareIdx].value == 0)
          $scope.data.splice(squareIdx, 1);
      } else {
        $scope.data.push({
          project: proj,
          date: gd(new Date(day)),
          value: 1
        });
      }

      $scope.data = _.reject($scope.data, function(d) { return d.value == 0 && d.project == proj; })
    }

    $scope.renameProject = function(proj, newName) {
      angular.forEach($scope.data, function(row) {
        if (row.project == proj)
          row.project = newName;
      });

      // $scope.heatmap[newName] = $scope.heatmap[proj];
      // delete $scope.heatmap[proj];
    }

    $scope.addProject = function() {
      $scope.data.push({
        project: 'New project',
        date: gd(new Date()),
        value: 0
      });

      // $scope.heatmap['New project'] = setupProject();
    }

    // var setupProject = function(minDate) {
    //   var obj = {};

    //   var tomorrow = new Date(); tomorrow.setDate(tomorrow.getDate() + 1);
    //   for (var date = minDate; date < tomorrow; date.setDate(date.getDate() + 1)) {
    //     obj[gd(date)] = 0;
    //   }

    //   return obj;
    // }

    var buildHeatmap = function(data) {
      var minDate = _.chain(data)
        .pluck('date')
        .map(function(d) { return new Date(d) })
        .min()
        .value();

      var monthAgo = new Date(); monthAgo.setDate(monthAgo.getDate() - 7);
      if (minDate > monthAgo)
        minDate = monthAgo;
      minDate = new Date(gd(minDate));

      // Plain list of project id's
      var projects = _.chain(data)
        .pluck('project')
        .uniq()
        .value();

      // Empty heatmap matrix
      var heatmap = {};
      // _.each(projects, function(proj) { heatmap[proj] = setupProject(minDate) });
      // var tomorrow = new Date(); tomorrow.setDate(tomorrow.getDate() + 1);
      for (var date = minDate; date < new Date(); date.setDate(date.getDate() + 1)) {
        _.each(projects, function(proj) {
          if (heatmap[proj] == undefined) 
            heatmap[proj] = {};
          heatmap[proj][gd(date)] = 0;
        });
      }

      // Populate heatmap matrix with values
      _.each(data, function(row) {
        var date = new Date(row.date);
        heatmap[row.project][gd(date)] = row.value;
      });   
      
      return heatmap;   
    }

    var buildMonthObj = function(heatmap) {
      if (Object.keys(heatmap).length == 0)
        return;

      var days = _.keys(heatmap[Object.keys(heatmap)[0]]);
      var mon = {};
      var prevMon = null;
      _.each(days, function(day) {
        if (prevMon != new Date(day).getMonth()) {
          mon[day] = new Date(day);
        } else {
          mon[day] = '';
        }
        prevMon = new Date(day).getMonth();
      });
      console.log('mon', mon);
      return mon;
    }

    $scope.$watch('data', function(nv) {
      $scope.heatmap = buildHeatmap(nv);
      $scope.monthObj = buildMonthObj($scope.heatmap);
      localStorage.setItem('data', JSON.stringify(nv));
    }, true);
  });